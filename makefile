OUT = bin/client-server
CC = g++
IFLAGS = -Iinclude
CFLAGS = -Wall -std=c++14
LDFLAGS = -lpthread
SOURCES = src/*

OBJECTS = $(SOURCES:.c=.o)

all:
	mkdir bin
	$(CC) $(IFLAGS) $(CFLAGS) -o $(OUT) $(SOURCES) $(LDFLAGS)
clean:
	rm -rf bin
