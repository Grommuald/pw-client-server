#include "Application.hpp"
#include <iostream>

int main( int argc, char** argv )
{
    const uint16_t NUMBER_OF_CLIENTS = 8;

	Application::Run( NUMBER_OF_CLIENTS );
	std::cin.get();

	return 0;
}
