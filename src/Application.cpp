#include "Application.hpp"
#include "Utilities.hpp"
#include <iostream>

bool Application::m_isRunning { true };
std::vector< std::shared_ptr< Client > > Application::m_clients;
std::shared_ptr< Server > Application::m_server;

void Application::Run( const size_t& numberOfClients )
{
	m_clients.resize( numberOfClients );

	m_server = std::make_shared< Server >();
	std::generate_n( m_clients.begin(), numberOfClients,
	[=]
	{
		auto clientPtr = std::make_shared< Client >( m_server );
		m_server->AddClient( clientPtr );

		return clientPtr;
	} );

	for ( auto& i : m_clients )
		i->GetThread().join();

	std::cout << "All client threads have been finished.\n";
	m_server->GetThread().join();
	std::cout << "Server thread has been finished.\n";
}
