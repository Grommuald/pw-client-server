#include "Server.hpp"
#include "Utilities.hpp"
#include "Client.hpp"
#include <iostream>
#include <random>

Server::Server()
{
	m_thread = std::thread( [=] {
		Run();
	} );
}

Server::~Server()
{
}

std::string Server::GetRandomClientId( const std::string& myId )
{
	std::lock_guard<std::mutex> lock( CommunicationNode::s_randomizationMutex );

	uint32_t index = 0;
	index = Utilities::GetRandomInt( 0, m_allClients.size() - 1 );

	return m_allClients[ index ];
}

void Server::AddClient( std::shared_ptr< Client > client )
{
	m_connectedClients[client->GetId()] = client;
	m_allClients.push_back( client->GetId() );
}

void Server::Run()
{
	std::cout << "Starting server...\n";
	while ( CheckIfRunning() )
	{
		m_sendDataTimer.ExecuteAfterTime( SEND_DATA_INTERVAL_IN_MILLIS,
		[this]
		{
			if ( auto currentData = Receive() )
			{
				m_active = true;

				auto client = m_connectedClients[currentData->receiverID];
				{
					std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
					std::cout << "Server: Sending data from client " << currentData->senderID
						<< " to client - " << currentData->receiverID << std::endl;
				}

				if ( client->CheckIfRunning() )
					Send( client, currentData );
				else
				{
					std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
					std::cout << "Server: Could not reach client " << currentData->receiverID << std::endl;
				}
			}
			else
			{
				std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
				std::cout << "Server: No data in queue.\n";
				if ( m_active )
					m_isUp = false;
			}
		} );
	}
    std::cout << "Closing server...\n";
}
