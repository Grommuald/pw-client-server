#include "Client.hpp"
#include "Server.hpp"
#include <iostream>

Client::Client( std::shared_ptr<Server>& server )
	:
    m_isTryingToConnect( true ),
	m_server( server )
{
	m_thread = std::thread( [=] {
		Run();
	} );
}

Client::~Client()
{
}

void Client::Run()
{
	while ( m_isTryingToConnect )
	{
		m_connectionTimer.ExecuteAfterTime(
			Utilities::GetRandomInt( CONNECT_AFTER_LOWER_BRACKET_IN_MILLIS, CONNECT_AFTER_UPPER_BRACKET_IN_MILLIS ),
		[this]
		{
			m_isTryingToConnect = false;
		} );
	}

	{
		std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
		std::cout << "Starting client: " << m_id << "\n";
	}
	while ( CheckIfRunning() )
	{
		m_disconnectionTimer.ExecuteAfterTime(
			Utilities::GetRandomInt( DISCONNECT_AFTER_LOWER_BRACKET_IN_MILLIS, DISCONNECT_AFTER_UPPER_BRACKET_IN_MILLIS ),
			std::bind( &Client::Disconnect, this ) );

		m_sendDataTimer.ExecuteAfterTime( SEND_DATA_INTERVAL_IN_MILLIS,
		[this]
		{
			std::unique_ptr< Data > data =
				std::make_unique< Data > ( GetId(), m_server->GetRandomClientId( GetId() ) );
			{
				std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
				std::cout << "Client " << data->senderID
					<< ": sending data to client " << data->receiverID << std::endl;
			}

			Send( m_server, data );
		} );
		if ( auto receivedData = Receive() )
		{
			std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
			std::cout << "Client " << GetId() << ": receiving data - "
				<< receivedData->data << " from client " << receivedData->senderID << "\n";
		}
	}
}

void Client::Disconnect()
{
	{
		std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
		std::cout << "Client " << GetId() << ": disconnected\n";
	}
	m_isUp = false;
}
