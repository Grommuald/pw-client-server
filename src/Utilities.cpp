#include "Utilities.hpp"

std::default_random_engine Utilities::e( std::random_device { }( ) );

uint32_t Utilities::GetRandomInt( const uint32_t& a, const uint32_t& b )
{
	std::uniform_int_distribution<uint32_t> u( a, b );
	return u( e );
}

std::string Utilities::GetRandomString( const size_t& length )
{
	static std::string charSet = "ABCDEFGHIJKLMNOPRSTUVWXYZabcdefghijklmnoprstuvwxyz";
	std::string result( length, 0 );

	std::generate_n( result.begin(), length,
	[=]
	{
		return charSet[ GetRandomInt( 0, length - 1 ) ];
	} );

	return result;
}
