#include "Timer.hpp"
#include <chrono>

Timer::Timer()
{
	m_hasBeenSet = false;
	m_stopTimer = false;
}

void Timer::ExecuteAfterTime( const int64_t & timeInMilliseconds, std::function<void()> trigger )
{
	if ( !m_stopTimer )
	{
		if ( !m_hasBeenSet )
		{
			m_timeToExecute = timeInMilliseconds;
			m_hasBeenSet = true;
			m_elapsedTime = std::chrono::system_clock::now();
		}
		else
		{
			auto nowMillis = std::chrono::time_point_cast< std::chrono::milliseconds >( std::chrono::system_clock::now() );
			auto startMillis = std::chrono::time_point_cast< std::chrono::milliseconds >( m_elapsedTime );

			auto durationStart =
                std::chrono::duration_cast< std::chrono::milliseconds > ( startMillis.time_since_epoch() ).count();
			auto durationNow =
                std::chrono::duration_cast< std::chrono::milliseconds > ( nowMillis.time_since_epoch() ).count();

			if ( durationNow >= durationStart + m_timeToExecute )
			{
				trigger();
				Reset();
			}
		}
	}
}

void Timer::Reset()
{
	m_hasBeenSet = false;
	m_stopTimer = false;
}
