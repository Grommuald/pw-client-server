#include "CommunicationNode.hpp"
#include <iostream>

std::mutex CommunicationNode::s_outputMutex;
std::mutex CommunicationNode::s_randomizationMutex;

CommunicationNode::CommunicationNode()
{
	m_id = std::to_string( reinterpret_cast< uint64_t >( this ) );
	{
		std::lock_guard<std::mutex> lock( CommunicationNode::s_outputMutex );
		std::cout << "Initilizing node: " << m_id << "\n";
	}
}

CommunicationNode::~CommunicationNode()
{
}

void CommunicationNode::AddToMessagesQueue( std::unique_ptr< Data >& data )
{
	m_incomingMessages.push_back( std::move( data ) );
}

bool CommunicationNode::Send( std::shared_ptr< CommunicationNode > receiver, std::unique_ptr< Data >& data )
{
	if ( receiver->CheckIfRunning() )
		receiver->AddToMessagesQueue( data );
	else
		return true;

	return false;
}

void CommunicationNode::Run()
{

}

std::unique_ptr< Data > CommunicationNode::Receive()
{
	if ( m_incomingMessages.empty() )
		return nullptr;

	auto data = std::move( m_incomingMessages.front() );
	m_incomingMessages.pop_front();
	return data;
}
