#include "Utilities.hpp"
#include "Timer.hpp"

#ifndef COMMUNICATION_NODE_HPP
#define COMMUNICATION_NODE_HPP

struct Data
{
	Data( const std::string& sender, const std::string& receiver )
		:
		senderID( sender ),
		receiverID( receiver )
	{
		data = Utilities::GetRandomString( DATA_SIZE );
	}
	static const uint32_t DATA_SIZE = 32;

	std::string senderID;
	std::string receiverID;
	std::string data;
};

class CommunicationNode
{
public:
	CommunicationNode();
	virtual ~CommunicationNode();

	virtual void Run();

	void AddToMessagesQueue( std::unique_ptr< Data >& );
	bool Send( std::shared_ptr< CommunicationNode >, std::unique_ptr< Data >& );
	std::unique_ptr< Data > Receive();

	bool CheckIfRunning() const { return m_isUp; }
	std::string GetId() const { return m_id; }
	std::thread& GetThread() { return m_thread; }

protected:
	static std::mutex s_outputMutex;
	static std::mutex s_randomizationMutex;

	bool m_isUp { true };
	Timer m_sendDataTimer;
	std::string m_id;
	std::deque< std::unique_ptr< Data > > m_incomingMessages;
	std::thread m_thread;
};

#endif
