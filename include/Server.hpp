#include "CommunicationNode.hpp"
#include "Timer.hpp"

#include <map>
#include <string>
#include <thread>
#include <deque>

#ifndef SERVER_HPP
#define SERVER_HPP

class Client;

class Server : public CommunicationNode
{
public:
	Server();
	~Server();

	std::string GetRandomClientId( const std::string& );
	void AddClient( std::shared_ptr< Client > );
	void Run();

private:
    enum { SEND_DATA_INTERVAL_IN_MILLIS = 1000 };
	bool m_active { false };
	std::map< std::string, std::shared_ptr< Client > > m_connectedClients;
	std::vector< std::string > m_allClients;
};

#endif
