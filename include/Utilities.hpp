#include <string>
#include <memory>
#include <algorithm>
#include <deque>
#include <vector>
#include <thread>
#include <mutex>
#include <random>

#ifndef UTILITIES_HPP
#define UTILITIES_HPP

class Utilities
{
public:
	static uint32_t GetRandomInt( const uint32_t&, const uint32_t& );
	static std::string GetRandomString( const size_t& );

private:
	static std::default_random_engine e;
};

#endif 
