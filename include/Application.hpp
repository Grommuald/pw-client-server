#include "Client.hpp"
#include "Server.hpp"

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

class Application
{
public:
	static void Run( const size_t& );

private:
	static bool m_isRunning;
	static std::vector< std::shared_ptr< Client > > m_clients;
	static std::shared_ptr< Server > m_server;
};

#endif
