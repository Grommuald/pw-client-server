#include "CommunicationNode.hpp"

#ifndef CLIENT_HPP
#define CLIENT_HPP

class Server;

class Client : public CommunicationNode
{
public:
	Client( std::shared_ptr< Server >& );
	~Client();

	void Run();
	void Disconnect();

private:
	enum
	{
        SEND_DATA_INTERVAL_IN_MILLIS = 5000,
		CONNECT_AFTER_LOWER_BRACKET_IN_MILLIS = 1000,
		CONNECT_AFTER_UPPER_BRACKET_IN_MILLIS = 10000,
		DISCONNECT_AFTER_LOWER_BRACKET_IN_MILLIS = 30000,
		DISCONNECT_AFTER_UPPER_BRACKET_IN_MILLIS = 60000,
	};

	bool m_isTryingToConnect;

	std::shared_ptr< Server > m_server;
	Timer m_connectionTimer;
	Timer m_disconnectionTimer;
};

#endif
